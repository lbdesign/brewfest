<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Brewfest
 */

?>
		</div><!-- .wrap -->
	</div><!-- #content -->

	<?php if ( is_active_sidebar( 'before-footer-1' ) ) { ?>
		<aside id="before-footer" class="widget-area" role="complementary">
			<div class="wrap">
				<?php dynamic_sidebar( 'before-footer-1' ); ?>
			</div>
		</aside><!-- #before-footer -->
	<?php } ?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="row row-1">
			<div class="wrap">
				<?php if ( is_active_sidebar( 'footer-1' ) ) { ?>
					<aside id="footer-1" class="widget-area" role="complementary">
							<?php dynamic_sidebar( 'footer-1' ); ?>
					</aside><!-- #footer-1 -->
				<?php } ?>
				<?php if ( is_active_sidebar( 'footer-2' ) ) { ?>
					<aside id="footer-2" class="widget-area" role="complementary">
							<?php dynamic_sidebar( 'footer-2' ); ?>
					</aside><!-- #footer-2 -->
				<?php } ?>
				<?php if ( is_active_sidebar( 'footer-3' ) ) { ?>
					<aside id="footer-3" class="widget-area" role="complementary">
							<?php dynamic_sidebar( 'footer-3' ); ?>
					</aside><!-- #footer-3 -->
				<?php } ?>
			</div><!-- .wrap -->
		</div><!-- .row -->
		<div class="row row-2">
			<div class="wrap">
				<p class="site-info">
					<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'brewfest' ) ); ?>"><?php printf( esc_html__( 'Happily built on %s', 'brewfest' ), 'WordPress' ); ?></a>
					<span class="sep"> | </span>
					<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'brewfest' ), 'brewfest', '<a href="http://lbdesign.tv" rel="designer">LBDesign</a>' ); ?>
				</p><!-- .site-info -->
				<?php wp_nav_menu( array(
					'theme_location' => 'footer',
					'menu_id' => 'footer-menu',
					'fallback_cb' => false,
					'container_class' => 'footer-menu-container',
					'after' => '<span class="menu-item-separator"> | </span>',
				) ); ?>
			</div><!-- .wrap -->
		</div><!-- .row -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>

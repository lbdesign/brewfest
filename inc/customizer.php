<?php
/**
 * Brewfest Theme Customizer.
 *
 * @package Brewfest
 */

 /**
  * Add postMessage support for site title and description for the Theme Customizer.
  *
  * @param WP_Customize_Manager $wp_customize Theme Customizer object.
  */
function brewfest_customize_register( $wp_customize ) {

	$wp_customize->get_setting( 'blogname' )->transport        = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector' => '.site-title a',
			'container_inclusive' => false,
			'render_callback' => 'brewfest_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector' => '.site-description',
			'container_inclusive' => false,
			'render_callback' => 'brewfest_customize_partial_blogdescription',
		) );
	}

	/**
	* Add primary color
	**/
	$wp_customize->add_setting( 'brewfest_color_primary', array(
		'default'           => '#F53D0A',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport'         => 'refresh',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'brewfest_color_primary', array(
		'label'       => __( 'Primary Color', 'brewfest' ),
		'description' => __( 'Applied to buttons and links.', 'brewfest' ),
		'section'     => 'colors',
	) ) );

	/**
	* Remove core header textcolor and background color as it uses the secondary color.
	**/
	$wp_customize->remove_control( 'header_textcolor' );
	$wp_customize->remove_control( 'background_color' );

	/**
	* Add secondary color
	**/
	$wp_customize->add_setting( 'brewfest_color_secondary', array(
		'default'           => '#220901',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport'         => 'refresh',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'brewfest_color_secondary', array(
		'label'       => __( 'Secondary Color', 'brewfest' ),
		'description' => __( 'Applied to text in header and footer and background.', 'brewfest' ),
		'section'     => 'colors',
	) ) );

	/**
	* Add tertiary color
	**/
	$wp_customize->add_setting( 'brewfest_color_tertiary', array(
		'default'           => '#f6f5ef',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport'         => 'refresh',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'brewfest_color_tertiary', array(
		'label'       => __( 'Tertiary Color', 'brewfest' ),
		'description' => __( 'Applied to header and footer backround and main text.', 'brewfest' ),
		'section'     => 'colors',
	) ) );

	/**
	* Add customizer section for hero banner
	**/
	$wp_customize->add_section( 'brewfest_hero_options' , array(
    'title'      => __('Hero','brewfest'),
    'priority'   => 61,
	) );

	/**
	* Add hero title setting
	**/
	$wp_customize->add_setting( 'brewfest_hero_title', array(
		'default'     => '',
		'transport'   => 'refresh',
    'sanitize_callback' => 'sanitize_text_field',
	) );

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'brewfest_hero_title', array(
		'label'       => __( 'Hero Title', 'brewfest' ),
		'section'     => 'brewfest_hero_options',
	) ) );

	/**
	* Add hero description setting
	**/
	$wp_customize->add_setting( 'brewfest_hero_description', array(
		'default'     => '',
		'transport'   => 'refresh',
    'sanitize_callback' => 'brewfest_sanitize_hero_description',
	) );

	$wp_customize->add_control(	'brewfest_hero_description', array(
		'label'    => __( 'Hero Description', 'brewfest' ),
		'section'  => 'brewfest_hero_options',
		'settings' => 'brewfest_hero_description',
		'type'     => 'textarea',
	) );

 /**
 * Add hero button text setting
 **/
	$wp_customize->add_setting( 'brewfest_hero_button_txt', array(
		'default'     => __( 'Button Text', 'brewfest' ),
		'transport'   => 'refresh',
    'sanitize_callback' => 'sanitize_text_field',
	) );

	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'brewfest_hero_button_txt', array(
		'label'       => __( 'Hero Button Text', 'brewfest' ),
		'section'     => 'brewfest_hero_options',
	) ) );

	/**
  * Add hero button link setting
  **/
	$wp_customize->add_setting( 'brewfest_hero_button_link', array(
		'default'     => '',
		'transport'   => 'refresh',
    'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control(
	'brewfest_hero_button_link',
		array(
			'label'    => __( 'Button Link', 'brewfest' ),
			'section'  => 'brewfest_hero_options',
			'settings' => 'brewfest_hero_button_link',
			'type'     => 'dropdown-pages',
		)
 );

}
add_action( 'customize_register', 'brewfest_customize_register', 11 );

/**
 * Sanitize the hero description
 */
function brewfest_sanitize_hero_description($value) {
  // $allowed = wp_kses_allowed_html( $value );

  $allowed = array(
    'a' => array(
        'href' => array(),
        'title' => array()
    ),
    'br' => array(),
    'em' => array(),
    'strong' => array(),
  );
	$value = wp_kses( $value, $allowed );

  return $value;

}

/**
 * Render the site title for the selective refresh partial.
 */
function brewfest_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 */
function brewfest_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Enqueues front-end CSS for colors.
 *
 * @since Brewfest 1.0
 */
function brewfest_color_css() {
	$color_primary = get_theme_mod( 'brewfest_color_primary' );
	$color_secondary = get_theme_mod( 'brewfest_color_secondary' );
	$color_tertiary = get_theme_mod( 'brewfest_color_tertiary' );

	// Don't do anything if the default colors are selected.
	if ( '#F53D0A' === $color_primary || false == $color_primary || '#220901' === $color_secondary || false == $color_secondary || '#f6f5ef' === $color_tertiary || false == $color_tertiary ) {
		return;
	}

	// Convert hex colors to rgba.
	$color_primary_rgb         	= brewfest_hex2rgb( $color_primary );
	$color_secondary_rgb 				= brewfest_hex2rgb( $color_secondary );
	$color_tertiary_rgb 				= brewfest_hex2rgb( $color_tertiary );
	$colors = array(
		'color_primary'            		=> $color_primary,
		'color_primary_dark'					=> vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.8)', $color_primary_rgb ),
		'color_secondary'				     	=> $color_secondary,
		'color_secondary_button_bg'		=> vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.7)', $color_secondary_rgb ),
		'color_secondary_footer_bg'		=> vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.94)', $color_secondary_rgb ),
		'color_secondary_widget_border' => vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.3)', $color_secondary_rgb ),
		'color_secondary_meta'				=> vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.7)', $color_secondary_rgb ),
		'color_secondary_branding'		=> vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.8)', $color_secondary_rgb ),
		'color_tertiary'        			=> $color_tertiary,
		'color_tertiary_light'				=> vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.5)', $color_tertiary_rgb ),
    'color_tertiary_sticky'       => vsprintf( 'rgba( %1$s, %2$s, %3$s, 0.9)', $color_tertiary_rgb ),
	);

	$color_scheme_css = brewfest_get_color_css( $colors );

	wp_add_inline_style( 'brewfest-style', $color_scheme_css );
}
add_action( 'wp_enqueue_scripts', 'brewfest_color_css' );

/**
 * Returns CSS for the color schemes.
 *
 * @since Twenty Fifteen 1.0
 *
 * @param array $colors Color scheme colors.
 * @return string Color scheme CSS.
 */
function brewfest_get_color_css( $colors ) {
	$colors = wp_parse_args( $colors, array(
		'color_primary'            		=> '',
		'color_primary_dark'					=> '',
		'color_secondary'				     	=> '',
		'color_secondary_button_bg'		=> '',
		'color_secondary_footer_bg'		=> '',
		'color_secondary_widget_border' => '',
		'color_secondary_meta'				=> '',
		'color_secondary_branding'		=> '',
		'color_tertiary'        			=> '',
		'color_tertiary_light'				=> '',
    'color_tertiary_sticky'				=> '',
	) );

	$css = <<<CSS
	/* Colors */

	body,
	button,
	input,
	select,
	textarea {
		color: {$colors['color_secondary']};
	}

	.entry-title a,
	.entry-title a:visited {
		color: {$colors['color_primary']};
	}

	blockquote::before {
		color: {$colors['color_primary']};
	}

	thead {
		background: {$colors['color_primary']};
		color: {$colors['color_tertiary']};
	}

  td, th {
    border-color: {$colors['color_secondary']};
  }

	pre {
		background: {$colors['color_secondary']};
		color: {$colors['color_tertiary']};
	}

	body {
		background: {$colors['color_tertiary_light']};
	}

	.site-content .wrap {
		background: {$colors['color_tertiary']};
	}

	.ft-button {
	  border-color: {$colors['color_primary']};
		color: {$colors['color_primary']};
		background: {$colors['color_secondary_button_bg']};
	}

	.ft-button::before {
		border-color: {$colors['color_primary']};
	}

	.ft-button:hover {
		border-color: {$colors['color_primary_dark']};
		color: {$colors['color_primary_dark']};
	}

	.ft-button:hover::before {
		border-color: {$colors['color_primary_dark']};
		color: {$colors['color_primary_dark']};
	}

  .button,
  button,
	input[type="button"],
	input[type="reset"],
	input[type="submit"] {
		color: {$colors['color_tertiary']};
		background: {$colors['color_primary']};
	}

  .button:hover,
  button:hover,
	input[type="button"]:hover,
	input[type="reset"]:hover,
	input[type="submit"]:hover,
  .button:active,
  button:active,
	input[type="button"]:active,
	input[type="reset"]:active,
	input[type="submit"]:active,
  .button:focus,
  button:focus,
	input[type="button"]:focus,
	input[type="reset"]:focus,
	input[type="submit"]:focus {
		background: {$colors['color_primary_dark']};
	}

	a:hover,
	a:focus,
	a:active {
		color: {$colors['color_primary_dark']};
	}

	.main-navigation {
		background: {$colors['color_secondary']};
	}

	.main-navigation a:hover {
		color: {$colors['color_primary_dark']};
	}

  .main-navigation .sub-menu {
    background: {$colors['color_secondary']};
  }

	.main-navigation ul ul {
		background: {$colors['color_secondary']};
	}

	.main-navigation .current_page_item > a,
	.main-navigation .current-menu-item > a,
	.main-navigation .current_page_ancestor > a,
	.main-navigation .current-menu-ancestor > a {
		background: {$colors['color_primary']};
	}

	.main-navigation .current_page_item > a:hover,
	.main-navigation .current-menu-item > a:hover,
	.main-navigation .current_page_ancestor > a:hover,
	.main-navigation .current-menu-ancestor > a:hover,
	.main-navigation .current_page_item > a:active,
	.main-navigation .current-menu-item > a:active,
	.main-navigation .current_page_ancestor > a:active,
	.main-navigation .current-menu-ancestor > a:active,
	.main-navigation .current_page_item > a:focus,
	.main-navigation .current-menu-item > a:focus,
	.main-navigation .current_page_ancestor > a:focus,
	.main-navigation .current-menu-ancestor > a:focus {
		background: {$colors['color_primary_dark']};
	}

	.menu-toggle {
		background: {$colors['color_primary']};
	}

	.menu-toggle:hover,
	.menu-toggle:active {
		color: {$colors['color_tertiary']};
	}

	.comment-navigation a,
	.posts-navigation a,
	.post-navigation a {
		color: {$colors['color_primary_dark']};
	}

	.site-header {
		background: {$colors['color_secondary']};
		color: {$colors['color_tertiary']};
	}

	.site-branding {
		background: {$colors['color_secondary_branding']};
	}

  .hentry.post.sticky {
  	background: {$colors['color_primary']};
  	color: {$colors['color_tertiary']};
  }

  .sticky .entry-title a,
  .sticky .entry-title a:visited {
  	color: {$colors['color_tertiary']};
  }

  .sticky .entry-meta,
  .sticky .entry-footer {
  	color: {$colors['color_tertiary_sticky']};
  }

  .sticky .read-more {
  	color: {$colors['color_tertiary']};
  }

	#before-footer {
		background: {$colors['color_primary']};
		color: {$colors['color_tertiary']};
	}

	.site-footer {
		background: {$colors['color_secondary_footer_bg']};
		color: {$colors['color_tertiary']};
	}

	.site-footer .row-2 {
		background: {$colors['color_secondary']};
	}

	.home.full-width .site-main .widget {
		border-color: {$colors['color_secondary_widget_border']};
	}

	.wp-caption {
		background: {$colors['color_tertiary']};
	}

	#wp-calendar a {
		color: {$colors['color_primary']};
	}

	.widget_recent_entries .post-date,
	.widget_rss .rss-date,
	.comment-metadata,
  .reply {
		color: {$colors['color_secondary_meta']};
	}

	.site-footer .widget_rss .rss-date,
	.site-footer .widget_recent_entries .post-date {
		color: {$colors['color_tertiary_light']};
	}

	.entry-meta,
	.entry-footer {
		color: {$colors['color_secondary_meta']};
	}

	.entry-content a {
		color: {$colors['color_primary']};
	}

CSS;

	return $css;
}

/**
 * Output an Underscore template for generating CSS for the color scheme.
 *
 * The template generates the css dynamically for instant display in the Customizer
 * preview.
 *
 * @since Twenty Fifteen 1.0
 */
function brewfest_color_css_template() {
	$colors = array(
		'color_primary'            		=> '{{ data.color_primary }}',
		'color_primary_dark'					=> '{{ data.color_primary_dark }}',
		'color_secondary'				     	=> '{{ data.color_secondary }}',
		'color_secondary_button_bg'		=> '{{ data.color_secondary_button_bg }}',
		'color_secondary_footer_bg'		=> '{{ data.color_secondary_footer_bg }}',
		'color_secondary_widget_border' => '{{ data.color_secondary_widget_border }}',
		'color_secondary_meta'				=> '{{ data.color_secondary_meta }}',
		'color_secondary_branding'		=> '{{ data.color_secondary_branding }}',
		'color_tertiary'        			=> '{{ data.color_tertiary }}',
		'color_tertiary_light'				=> '{{ data.color_tertiary_light }}',
	);
	?>
	<script type="text/html" id="tmpl-brewfest-color-scheme">
		<?php echo brewfest_get_color_css( $colors ); ?>
	</script>
	<?php
}
add_action( 'customize_controls_print_footer_scripts', 'brewfest_color_css_template' );

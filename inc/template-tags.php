<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Brewfest
 */

if ( ! function_exists( 'brewfest_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function brewfest_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( 'on %s', 'post date', 'brewfest' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'brewfest' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	$num_comments = get_comments_number(); // get_comments_number returns only a numeric value
	$write_comments = '';

	if ( ! post_password_required() && ( comments_open() ) ) {
		if ( $num_comments == 0 ) {
			$comments = __( 'No Comments', 'brewfest' );
		} elseif ( $num_comments > 1 ) {
			$comments = $num_comments . __( ' Comments', 'brewfest' );
		} else {
			$comments = __('1 Comment', 'brewfest');
		}
		$write_comments = '<span class="comments-count"> | <a href="' . get_comments_link() .'">'. $comments.'</a></span>';
	}

	echo '<span class="byline">' . $byline . ' </span><span class="posted-on">' . $posted_on . '</span>' . $write_comments; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'brewfest_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function brewfest_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'brewfest' ) );
		if ( $categories_list && brewfest_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Filed under: %1$s', 'brewfest' ) . '</span><br/>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'brewfest' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged: %1$s', 'brewfest' ) . '</span><br/>', $tags_list ); // WPCS: XSS OK.
		}
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'brewfest' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

if ( ! function_exists( 'brewfest_the_post_navigation' ) ) :
/**
 * Adds arguments for retrieving the next/previous post, when applicable
 */
function brewfest_the_post_navigation() {
  $args = array(
    'prev_text'          => '&laquo; %title',
    'next_text'          => '%title &raquo;',
  );

  the_post_navigation($args);
}

endif;

if ( ! function_exists( 'brewfest_the_posts_navigation' ) ) :
/**
 * Adds arguments for retrieving the next/previous page of posts, when applicable
 */
function brewfest_the_posts_navigation() {
  $args = array(
    'prev_text'          => __( '&laquo; Older posts', 'brewfest' ),
    'next_text'          => __( 'Newer posts &raquo;', 'brewfest' ),
		'screen_reader_text' => __( 'Posts navigation', 'brewfest' ),
  );

  the_posts_navigation($args);
}

endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function brewfest_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'brewfest_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'brewfest_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so brewfest_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so brewfest_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in brewfest_categorized_blog.
 */
function brewfest_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'brewfest_categories' );
}
add_action( 'edit_category', 'brewfest_category_transient_flusher' );
add_action( 'save_post',     'brewfest_category_transient_flusher' );

if ( ! function_exists( 'brewfest_the_custom_logo' ) ) :
/**
 * Displays the optional custom logo.
 *
 * Does nothing if the custom logo is not available.
 *
 * @since Brewfest 1.0
 */
function brewfest_the_custom_logo() {
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}
}
endif;

if ( ! function_exists( 'brewfest_excerpt' ) ) :
/**
 * Displays the optional excerpt.
 *
 * Wraps the excerpt in a div element.
 *
 * Create your own brewfest_excerpt() function to override in a child theme.
 *
 * @since Brewfest 1.0
 *
 * @param string $class Optional. Class string of the div element. Defaults to 'entry-summary'.
 */
function brewfest_excerpt( $class = 'entry-summary' ) {
	$class = esc_attr( $class );
	$brewfest_excerpt = '';

	if ( has_excerpt() || is_search() ) {
		$brewfest_excerpt = '<div class="'. $class .'">';
		$brewfest_excerpt .= get_the_excerpt();
		$brewfest_excerpt .= '</div><!-- .'. $class .' -->';
	}

	return $brewfest_excerpt;
}
endif;

<?php
/**
* @link https://developer.wordpress.org/themes/functionality/custom-headers/
*
* @package Brewfest
*/

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses brewfest_header_style()
 */
function brewfest_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'brewfest_custom_header_args', array(
		'default-image'					 => get_template_directory_uri() . '/images/header-tap.jpg',
		'width'                  => 1300,
		'height'                 => 954,
		'wp-head-callback'       => 'brewfest_header_style',
	) ) );
}
add_action( 'after_setup_theme', 'brewfest_custom_header_setup' );

// Default custom header packaged with the theme. %s is a placeholder for the theme template directory URI.
register_default_headers( array(
	'tap' => array(
		'url'           => '%s/images/header-tap.jpg',
		'thumbnail_url' => '%s/images/header-tap-thumbnail.jpg',
		'description'   => __( 'Tap', 'brewfest' )
	)
) );

/**
 * Convert HEX to RGB.
 *
 * @since Brewfest 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function brewfest_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) == 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) == 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

if ( ! function_exists( 'brewfest_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog.
 *
 * @since Twenty Fifteen 1.0
 *
 * @see brewfest_custom_header_setup()
 */
function brewfest_header_style() {
	$header_image = esc_url( get_header_image() );

	// If no custom options for text are set, let's bail.
	if ( empty( $header_image ) && display_header_text() ) {
		return;
	}

	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css" id="brewfest-header-css">
	<?php
		// Has a Custom Header been added?
		if ( ! empty( $header_image ) ) :
	?>
		.site-header {

			/*
			 * No shorthand so the Customizer can override individual properties.
			 * @see https://core.trac.wordpress.org/ticket/31460
			 */
			background-image: url(<?php header_image(); ?>);
			background-repeat: no-repeat;
			background-position: 50% 50%;
			-webkit-background-size: cover;
			-moz-background-size:    cover;
			-o-background-size:      cover;
			background-size:         cover;
		}

	<?php
		endif;

		// Has the text been hidden?
		if ( ! display_header_text() ) :
	?>
		.site-title,
		.site-description {
			clip: rect(1px, 1px, 1px, 1px);
			position: absolute;
		}
	<?php endif; ?>
	</style>
	<?php
}
endif; // brewfest_header_style

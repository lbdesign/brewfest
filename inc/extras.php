<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Brewfest
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function brewfest_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	if ( is_page_template('template-full-width.php') || ( is_active_sidebar( 'home-1' ) && is_front_page() && get_option( 'show_on_front' ) == 'page' ) ) {
		$classes[] = 'full-width';
	}

	return $classes;
}
add_filter( 'body_class', 'brewfest_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function brewfest_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', bloginfo( 'pingback_url' ), '">';
	}
}
add_action( 'wp_head', 'brewfest_pingback_header' );

function brewfest_excerpt_more( $more ) {
	if ( is_admin() ) {
		return $link;
	}
	
	return '... <a class="read-more" href="'. esc_url( get_permalink( get_the_ID() ) ) . '">' . __('Read More &raquo;', 'brewfest') . '</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

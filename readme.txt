=== Brewfest ===

Contributors: LBDesign
Tags: one-column, two-columns, left-sidebar, custom-background, custom-colors, custom-header, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, full-width-template, sticky-post, theme-options, threaded-comments, translation-ready, entertainment, food-and-drink


Requires at least: 4.5
Tested up to: 4.7
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Brewfest features multiple widget areas on the front page, navigation, a logo, custom header, hero banner area and more. Use the custom colors to match your event's colors and add an optional logo.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.
4. Customize it by going to Appearance -> Customize.

== Copyright ==

Brewfest WordPress Theme, Copyright 2017 LBDesign
Brewfest is distributed under the terms of the GNU GPL

== Changelog ==

= 1.0 =
* Released: December x, 2016

Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
* Bundled header image https://stocksnap.io/photo/CS5KND10L2/, (C) Maciej Korsan, [CC0 1.0 Universal (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/)

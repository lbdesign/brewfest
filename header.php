<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Brewfest
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'brewfest' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<div class="wrap">
					<?php brewfest_the_custom_logo(); ?>
					<?php
					if ( is_front_page() && is_home() ) : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
					endif; ?>
					<?php
					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
					<?php
					endif; ?>
			</div><!-- .wrap -->
		</div><!-- .site-branding -->

		<?php
		if ( is_front_page() ) : ?>
		<div class="header-hero">
			<div class="wrap">
				<div class="inner-wrap">
					<?php
					$brewfest_hero_title = get_theme_mod( 'brewfest_hero_title' );
					$brewfest_hero_description = get_theme_mod( 'brewfest_hero_description' );
					$brewfest_hero_button_txt = get_theme_mod( 'brewfest_hero_button_txt', __('Button Text', 'brewfest') );
					$brewfest_hero_button_link = get_theme_mod( 'brewfest_hero_button_link' );
					?>
					<?php if ( ! empty($brewfest_hero_title) ) { ?>
						<h2><?php echo esc_html($brewfest_hero_title) ?></h2>
					<?php } ?>
					<?php if ( ! empty($brewfest_hero_description) ) { ?>
						<div class="hero-description">
							<?php echo wp_kses_post($brewfest_hero_description) ?>
						</div>
					<?php } ?>
					<?php if ($brewfest_hero_button_txt && $brewfest_hero_button_link) {
						echo '<a href="'. esc_url(get_permalink($brewfest_hero_button_link)) . '" class="ft-button ft-button-hero">' . esc_html($brewfest_hero_button_txt) . '</a>';
					}
					?>
				</div><!-- .inner-wrap -->
			</div><!-- .wrap -->
		</div><!-- .header-options -->
		<?php endif; ?>

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<div class="wrap">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'brewfest' ); ?></button>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			</div><!-- .wrap -->
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
		<div class="wrap">
